﻿using System;
using System.Linq;

namespace Git_practica02
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio01();
        }
        public static void ejercicio01()
        {
            //leer 10 notas y sacar numero de aprobados y sobresalientes
            int numaprobados = 0;
            int numsobresalientes = 0;
            double nota;

            Console.WriteLine("Introducir 10 notas");

            for (int i = 0; i < 10; i++)
            {
                nota = double.Parse(Console.ReadLine());
                if (nota>=5)
                {
                    numaprobados++;
                }
                if (nota >=9)
                {
                    numsobresalientes++;
                }
            }
            //presentar la solucion
            Console.WriteLine("El numero de aprobados es: " + numaprobados + " y el numero de sobresalientes " + numsobresalientes);
        }
        public static void ejercicio02()
        {
            //Leer fecha de recepcion factura
            int dia, mes, ano, mesnuevo, aux;
            Console.WriteLine("Introducir una fecha en formato dia, mes, año");
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            ano = Int32.Parse(Console.ReadLine());
            mesnuevo = mes + 3;
            //Comprobar que el dia sea mayor que 0 y menor a 31 
            //Comprobar que el mes sea mayor a 1 y menor a 12

            if ((dia) <1|| dia >30 || mes >12|| mes <1 )
            {
                Console.WriteLine("No has insertado una fecha correctamente");
            }
            else
            {
                //La fecha es correcta, añadimos 3 meses
                if (mesnuevo > 12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                }
            }
            Console.WriteLine("La siguiente fecha de pago es: " + dia + "/" + mesnuevo + "/" + ano);
            
        }
        public static void ejercicio03()
        {
            int[] numeros = new int[10];
            numeros[0] = 5;
            numeros[1] = 6;
            numeros[2] = 2;
            numeros[3] = 8;
            numeros[4] = 15;
            numeros[5] = 9;
            numeros[6] = 1;
            numeros[7] = 4;
            numeros[8] = 7;
            numeros[9] = 11;

            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine(numeros[i]);
            }
            int e = 10;
            while (e!=0)
            {
                Console.WriteLine("Numeros invertidos");
                for (int i = 9; i >= 0; i--)
                {
                    Console.WriteLine(numeros [i]);
                    e--;
                }
            } 
            
        }
        public static void ejercicio05()
        {
            int[] valores = new int[10];
            int nummayor1 = 0;
            int nummayor2 = 0;
            Console.WriteLine("Introduce 10 valores numericos");
            for (int i = 0; i < valores.Length; i++)
            {
                valores[i] = Int32.Parse(Console.ReadLine());
                if (valores[i]>nummayor1)
                {
                    nummayor1 = valores[i];
                }
                if ((valores[i] > nummayor2) && (valores[i] < nummayor1))
                {
                    nummayor2 = valores[i];
                }
            }
            Console.WriteLine("El 1er numero mayor es: " + nummayor1);
            Console.WriteLine("El 2er numero mayor es: " + nummayor2);

        }
        public static void ejercicio06()
        {
            int[] numeros = new int[10];
            int a = 10;

            Console.WriteLine("Introduce 10 valores numericos");
            for (int i = 0; i < numeros.Length; i++)
            {
                numeros[i] = Int32.Parse(Console.ReadLine());
                a--;

            }
            Console.WriteLine("Total valores diferentes " + a);
        }
        public static void ejercicio07()
        {
            Console.WriteLine("Calificaciones del tribunal");
            int[] notas = new int[10];
            notas[0] = 5;
            notas[1] = 6;
            notas[2] = 2;
            notas[3] = 8;
            notas[4] = 8;
            notas[5] = 9;
            notas[6] = 4;
            notas[7] = 4;
            notas[8] = 7;
            notas[9] = 10;
            int mayor = 0;
            int menor = 0;
            double media = 0;

            Array.Sort(notas);
            for (int i = 0; i < notas.Length; i++)
            {
                Console.WriteLine(notas[i]);
            }
            if (mayor-menor>=3)
            {
                //Condicion:
                //Si nota mayor se diferencia de nota menor por 3 puntos, no tomarlos en cuenta.
                media = notas.Sum() - mayor;
                media = media - menor;
                media = media/ 8;
                Console.WriteLine("La nota media es: " + media);
            }
            else
            {
                media = notas.Sum() / 10;
                Console.WriteLine("La nota media es: " + media);
            }
        }
    }
}
